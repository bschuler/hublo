import { Component, OnInit } from '@angular/core';
import { FaceSnap } from './models/face-snap.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  faceSnaps!: FaceSnap[];

  ngOnInit(){
    this.faceSnaps = [
      {
        title: 'Akroma',
        description: 'Ange de la fureur',
        imageUrl: 'https://www.smfcorp.net/images/artworks/big/6743.jpg',
        createdDate: new Date()
      },
      {
        title: 'Akroma',
        description: 'Ange de la colère',
        imageUrl: 'https://www.smfcorp.net/images/artworks/big/6757.jpg',
        createdDate: new Date()
      },
      {
        title: 'Akroma',
        description: 'Vison D\'Ixidor',
        imageUrl: 'https://www.smfcorp.net/images/artworks/big/17842.jpg',
        createdDate: new Date(),
        location: 'Commander Legends'
      }
    ];
  }
}
