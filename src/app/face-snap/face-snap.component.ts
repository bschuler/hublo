import { Component, OnInit, Input } from '@angular/core';
import { FaceSnap } from '../models/face-snap.model';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent implements OnInit {
  @Input() faceSnap!: FaceSnap;

  snaps!: number;
  snapped!: boolean;

  ngOnInit() {
    this.snaps = 0;
    this.snapped = false;
  }

  onSnap() {
    if (!this.snapped) {
      this.snaps++;
      this.snapped = true;
    }
    else{
      this.snaps--;
      this.snapped=false;
    }
    
  }
  
}
